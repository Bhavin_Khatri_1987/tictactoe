import { Component, OnInit } from '@angular/core';
import { Player } from './actors/player';
import { Referee } from './actors/referee';
import { Cell } from './board/cell';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // Board width i.e. 3x3 in this case
  boardWidth = 3;
  cells = [];

  // Total number of moves that is possible on the board
  totalMoves: number;

  // Total number of moves played by players
  playedMove: number;
  constructor() {
    this.totalMoves = this.boardWidth * this.boardWidth;
    this.playedMove = 0;
    for (let index = 0; index < this.totalMoves; index++) {
      let cell = new Cell(index + 1);
      this.cells.push(cell);
    }
  }

  ngOnInit(): void {
    this.initializePlayers();
    this.referee = new Referee(this.players);
  }
  title = 'Tic-Tac-Toe';
  players: Array<Player> = [];
  referee: Referee;

  /**
   * Initializes two players with their symbols
   */
  private initializePlayers() {
    let player1 = new Player("Player 1", "X");
    player1.isPlaying = true;
    this.players.push(player1);
    this.players.push(new Player("Player 2", "0"));
  }

  /**
   * Sets player's move on cell inside board 
   * Decision will be made based on winning conditions by referee
   * @param cell selected by player in charge
   */
  onCellClick(cell: Cell) {
    // If cell do not have symbol then add symbol of the player 
    // otherwise show message about invalid move
    if (!cell.symbol) {
      let playerInCharge = this.referee.PlayerInCharge;
      playerInCharge.setSymbolOnBoard(cell);
      playerInCharge.recordMove(cell.id);
      this.playedMove++;

      // If condition is true then declare the player in charge a winner of the game 
      if (this.referee.verifyWinningCondition(playerInCharge)) {

        // Set time out to reflact move by player on board
        setTimeout(() => {
          alert(playerInCharge.name + " Wins.");
          this.resetBoard();
        });
      } else if (this.playedMove === this.totalMoves) {
        setTimeout(() => {
          alert("Draw !!!");
          this.resetBoard();
        });
        
      } else {
        // If result is not decided then toggle the player
        this.referee.togglePlayer();

      }
    } else {
      alert("Invalid Move");
    }
  }

  /**
   * Reset the board, players, played moves on board
   */
  resetBoard() {
    this.cells.forEach(cell => {
      cell.symbol = null;
    });
    this.resetPlayers();
    this.playedMove = 0;
    this.referee = new Referee(this.players);
  }

  /**
   * Resets players statistics
   */
  private resetPlayers() {
    this.players.forEach(p => {
      p.resetPlayerStats();
    });
  }
}
