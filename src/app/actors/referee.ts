import { Player } from './player';

export class Referee {
    players: Array<Player> = [];
    playerInChargeIndex: number;
    winningConditions: Array<Array<number>>;
    constructor(players: Array<Player>) {
        this.players = players;
        this.playerInChargeIndex = 0;
        if (this.players.length > 0) {
            this.setPlayerInCharge();
        }

        this.winningConditions = [
            [1, 2, 3], [4, 5, 6], [7, 8, 9], // Row winning conditions
            [1, 5, 9], [3, 5, 7], // Diagonal winning conditions
            [1, 4, 7], [2, 5, 8], [3, 6, 9] // Column winning conditions
        ];
    }

    togglePlayer() {
        this.playerInChargeIndex = this.playerInChargeIndex === 0 ? 1 : 0;
        this.setPlayerInCharge();
    }

    /**
     * verifies moves of player against winning conditions and decides if player 
     * has 3 consecutive counts from winning condition if it is then true will be returned
     * @param player player to verify winner
     */
    verifyWinningCondition(player: Player): boolean {
        let consecutiveCounts = 0;
        for (let i = 0; i < this.winningConditions.length && consecutiveCounts < 3; i++) {
            const winningConditions = this.winningConditions[i];
            for (let j = 0; j < winningConditions.length && consecutiveCounts < 3; j++) {
                const winningCondition = winningConditions[j];
                if (player.moves.some(x => x === winningCondition)) {
                    consecutiveCounts++;
                }
            }
            if (consecutiveCounts < 3) {
                consecutiveCounts = 0;
            }
        }
        return consecutiveCounts === 3;
    }

    /**
     * Sets player in charge defined by isPlaying property to true
     */
    setPlayerInCharge() {
        this.players.forEach(pl => {
            pl.isPlaying = false;
        });
        this.players[this.playerInChargeIndex].isPlaying = true;
    }

    get PlayerInCharge() {
        return this.players[this.playerInChargeIndex];
    }
}
