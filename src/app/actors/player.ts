import { Cell } from '../board/cell';

export class Player {
    name: string;
    symbol: string;
    isPlaying: boolean;
    moves: Array<number> = [];
    constructor(name: string, symbol?: string) {
        this.name = name;
        this.symbol = symbol;
    }

    setSymbolOnBoard(cell: Cell) {
        cell.symbol = this.symbol;
    }

    recordMove(cellId: number) {
        this.moves.push(cellId);
    }

    resetPlayerStats() {
        this.moves = [];
        this.isPlaying = false;
    }
}
