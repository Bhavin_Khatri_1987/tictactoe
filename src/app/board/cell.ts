export class Cell {
    id: number;
    cellElement: HTMLDivElement;
    symbol: string;
    constructor(id: number, cellElement?: HTMLDivElement) {
        this.id = id;
        this.cellElement = cellElement;
    }
}
