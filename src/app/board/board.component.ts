import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Cell } from './cell';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent {
  @Output() cellClick: EventEmitter<Cell> = new EventEmitter<Cell>();
  @Input() cells: Array<Cell>;


  onCellClick(cell: Cell) {
    this.cellClick.emit(cell);
  }

}
